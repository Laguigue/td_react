import React, { Component } from 'react';
import './App.css';
import Image from './Image';
import datas from './dummy';

class App extends Component {

	constructor() {
		super();
		this.state = {
			searchValue: '',
			datas: [],
      savedImages: []
		};
	}

	handleChange = (event) => {
		this.setState({searchValue: event.target.value});
	}

	saveImage = (image) => {
		var tempImagesSaved = this.state.savedImages;
		if (!this.state.datas.find(dataImage => dataImage.saved === true)) {
			tempImagesSaved.push(image);
			this.setState({savedImages: tempImagesSaved});
    };
		localStorage.setItem('images', JSON.stringify(this.state.savedImages));
	}

	componentDidMount() {
		this.setState({savedImages: JSON.parse(localStorage.getItem('images'))});
	}

	isImageSaved = (image) => {
		if (!this.state.savedImages.find(savedImage => savedImage.id === image.id)) {
			return true;
		};
  }

  render() {
    return (
      <div className="App">
        <input type="text" onChange={this.handleChange}/>
        <div className="images_list">
					{datas.map((image, index) => {
					  if (this.state.searchValue === '') {
							return <Image key={index} image={image} saveImage={this.saveImage} isImageSaved={this.isImageSaved(image)}/>
            } else if (image.username === this.state.searchValue) {
							return <Image key={index} image={image} saveImage={this.saveImage} isImageSaved={this.isImageSaved(image)}/>
            }
					})}
        </div>
      </div>
    );
  }
}

export default App;

/****/
