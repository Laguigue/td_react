import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Image extends Component {

	static propTypes = {
		image: PropTypes.object.isRequired,
		saveImage: PropTypes.func,
		isImageSaved: PropTypes.bool,
	};

	static defaultProps = {
		saveImage: () => {},
		isImageSaved: false
	};

  render() {

		const {image, saveImage, isImageSaved} = this.props;
		const className = isImageSaved ? "image" : "imageSaved";

    return (
      <div className={className} onClick={() => saveImage(image)} >
        <span>{image.username}</span>
        <img src={image.picture} alt=""/>
      </div>
    );
  }
}

export default Image;
